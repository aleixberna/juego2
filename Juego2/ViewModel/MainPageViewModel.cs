﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Juego2
{
    public class MainPageViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        CultureInfo cultureInfo = CultureInfo.CreateSpecificCulture("el-GR");

        //Declaro todos los icommand de los juegos
        public ICommand BotoLeft1 { get; set; }
        public ICommand BotoLeft2 { get; set; }
        public ICommand BotoLeft3 { get; set; }
        public ICommand BotoLeft4 { get; set; }
        public ICommand BotoLeft5 { get; set; }
        public ICommand BotoCentro { get; set; }
        public ICommand BotoRight1 { get; set; }
        public ICommand BotoRight2 { get; set; }
        public ICommand BotoRight3 { get; set; }
        public ICommand BotoRight4 { get; set; }
        public ICommand BotoRight5 { get; set; }
        public ICommand BotoReset { get; set; }

        //Declararo todas las variables

        public String left1 = "blava1.png";
        public String left2 = "blava2.png";
        public String left3 = "blava3.png";
        public String left4 = "blava4.png";
        public String left5 = "blava5.png";
        public String centro = "vacio.png";
        public String right1 = "verd1.png";
        public String right2 = "verd2.png";
        public String right3 = "verd3.png";
        public String right4 = "verd4.png";
        public String right5 = "verd5.png";
        public String imagenFondo = "pantano.png";
        public Boolean left1Enable = false;
        public Boolean left2Enable = false;
        public Boolean left3Enable = false;
        public Boolean left4Enable = true;
        public Boolean left5Enable = true;
        public Boolean centroEnable = false;
        public Boolean right1Enable = true;
        public Boolean right2Enable = true;
        public Boolean right3Enable = false;
        public Boolean right4Enable = false;
        public Boolean right5Enable = false;
        public Boolean flagbooluna = true;
        public Boolean visibleReset = false;
        public String flagcarta = "";
        public int numeroArray = 0;
        public int i = 0;
        public int z = 99;
        public int numClicks = 50;
        public int posicioEnable1 = 0;
        public int posicioEnable2 = 0;
        public int posicioEnable3 = 0;
        public int posicioEnable4 = 0;
        public int posicioBuida = 0;

        //Declaro los arrays que usaremos

        String[] ArrayRanas = new String[11];
        Boolean[] ArrayRanasBool = new Boolean[11];
        public String Left1
        {
            set
            {
                if (left1 != value)
                {
                    left1 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("Left1"));
                    }
                }
            }
            get { return left1; }
        }
        public String Left2
        {
            set
            {
                if (left2 != value)
                {
                    left2 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("Left2"));
                    }
                }
            }
            get { return left2; }
        }
        public String Left3
        {
            set
            {
                if (left3 != value)
                {
                    left3 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("Left3"));
                    }
                }
            }
            get { return left3; }
        }
        public String Left4
        {
            set
            {
                if (left4 != value)
                {
                    left4 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("Left4"));
                    }
                }
            }
            get { return left4; }
        }
        public String Left5
        {
            set
            {
                if (left5 != value)
                {
                    left5 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("Left5"));
                    }
                }
            }
            get { return left5; }
        }
        public String Centro
        {
            set
            {
                if (centro != value)
                {
                    centro = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("Centro"));
                    }
                }
            }
            get { return centro; }
        }
        public String Right1
        {
            set
            {
                if (right1 != value)
                {
                    right1 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("Right1"));
                    }
                }
            }
            get { return right1; }
        }
        public String Right2
        {
            set
            {
                if (right2 != value)
                {
                    right2 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("Right2"));
                    }
                }
            }
            get { return right2; }
        }
        public String Right3
        {
            set
            {
                if (right3 != value)
                {
                    right3 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("Right3"));
                    }
                }
            }
            get { return right3; }
        }
        public String Right4
        {
            set
            {
                if (right4 != value)
                {
                    right4 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("Right4"));
                    }
                }
            }
            get { return right4; }
        }
        public String Right5
        {
            set
            {
                if (right5 != value)
                {
                    right5 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("Right5"));
                    }
                }
            }
            get { return right5; }
        }
        public String ImagenFondo
        {
            set
            {
                if (imagenFondo != value)
                {
                    imagenFondo = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("ImagenFondo"));
                    }
                }
            }
            get { return imagenFondo; }
        }
        public int NumClicks
        {
            set
            {
                if (numClicks != value)
                {
                    numClicks = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("NumClicks"));
                    }
                }
            }
            get { return numClicks; }
        }
        public Boolean Left1Enable
        {
            set
            {
                if (left1Enable != value)
                {
                    left1Enable = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("Left1Enable"));
                    }
                }
            }
            get { return left1Enable; }
        }
        public Boolean Left2Enable
        {
            set
            {
                if (left2Enable != value)
                {
                    left2Enable = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("Left2Enable"));
                    }
                }
            }
            get { return left2Enable; }
        }
        public Boolean Left3Enable
        {
            set
            {
                if (left3Enable != value)
                {
                    left3Enable = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("Left3Enable"));
                    }
                }
            }
            get { return left3Enable; }
        }
        public Boolean Left4Enable
        {
            set
            {
                if (left4Enable != value)
                {
                    left4Enable = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("Left4Enable"));
                    }
                }
            }
            get { return left4Enable; }
        }
        public Boolean Left5Enable
        {
            set
            {
                if (left5Enable != value)
                {
                    left5Enable = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("Left5Enable"));
                    }
                }
            }
            get { return left5Enable; }
        }
        public Boolean CentroEnable
        {
            set
            {
                if (centroEnable != value)
                {
                    centroEnable = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("CentroEnable"));
                    }
                }
            }
            get { return centroEnable; }
        }
        public Boolean Right1Enable
        {
            set
            {
                if (right1Enable != value)
                {
                    right1Enable = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("Right1Enable"));
                    }
                }
            }
            get { return right1Enable; }
        }
        public Boolean Right2Enable
        {
            set
            {
                if (right2Enable != value)
                {
                    right2Enable = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("Right2Enable"));
                    }
                }
            }
            get { return right2Enable; }
        }
        public Boolean Right3Enable
        {
            set
            {
                if (right3Enable != value)
                {
                    right3Enable = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("Right3Enable"));
                    }
                }
            }
            get { return right3Enable; }
        }
        public Boolean Right4Enable
        {
            set
            {
                if (right4Enable != value)
                {
                    right4Enable = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("Right4Enable"));
                    }
                }
            }
            get { return right4Enable; }
        }
        public Boolean Right5Enable
        {
            set
            {
                if (right5Enable != value)
                {
                    right5Enable = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("Right5Enable"));
                    }
                }
            }
            get { return right5Enable; }
        }
        public Boolean VisibleReset
        {
            set
            {
                if (visibleReset != value)
                {
                    visibleReset = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("VisibleReset"));
                    }
                }
            }
            get { return visibleReset; }
        }

        public MainPageViewModel()
        {
            //Botones que llaman a las funciones
            BotoLeft1 = new Command(BotonLeft1);
            BotoLeft2 = new Command(BotonLeft2);
            BotoLeft3 = new Command(BotonLeft3);
            BotoLeft4 = new Command(BotonLeft4);
            BotoLeft5 = new Command(BotonLeft5);
            BotoCentro = new Command(BotonCentro);
            BotoRight1 = new Command(BotonRight1);
            BotoRight2 = new Command(BotonRight2);
            BotoRight3 = new Command(BotonRight3);
            BotoRight4 = new Command(BotonRight4);
            BotoRight5 = new Command(BotonRight5);
            BotoReset = new Command(ResetAll);
            ImagenFondo = "fondo1.png";

            //Funcion que solo se ejecuta una vez
            FuncionUnaVez();
        }
        public void FuncionUnaVez() {

            //Declarar cada imagen en el array
            if (flagbooluna == true)
            {
                ArrayRanas[0] = "blava1.png";
                ArrayRanas[1] = "blava2.png";
                ArrayRanas[2] = "blava3.png";
                ArrayRanas[3] = "blava4.png";
                ArrayRanas[4] = "blava5.png";
                ArrayRanas[5] = "vacio.png";
                ArrayRanas[6] = "verd1.png";
                ArrayRanas[7] = "verd2.png";
                ArrayRanas[8] = "verd3.png";
                ArrayRanas[9] = "verd4.png";
                ArrayRanas[10] = "verd5.png";
            }
            flagbooluna = false;
            VisibleReset = false;
        }
        //Funciones de cada rana i cada boton
        public void BotonLeft1()
        {
            flagcarta = ArrayRanas[0];   
            for (int i = 0; i < ArrayRanas.Length; i++)
            {
                if (ArrayRanas[i] == "vacio.png")
                {
                    z = i;
                    ArrayRanas[i] = flagcarta;
                }
            }
            ArrayRanas[0] = "vacio.png";
            AssignarLlocs();
        }
        public void BotonLeft2()
        {
            flagcarta = ArrayRanas[1];
            for (int i = 0; i < ArrayRanas.Length; i++)
            {
                if (ArrayRanas[i] == "vacio.png")
                {
                    z = i;
                    ArrayRanas[i] = flagcarta;
                }
            }
            ArrayRanas[1] = "vacio.png";
            AssignarLlocs();
        }
        public void BotonLeft3()
        {
            flagcarta = ArrayRanas[2];
            for (int i = 0; i < ArrayRanas.Length; i++)
            {
                if (ArrayRanas[i] == "vacio.png")
                {
                    z = i;
                    ArrayRanas[i] = flagcarta;
                }
            }
            ArrayRanas[2] = "vacio.png";
            AssignarLlocs();
        }
        public void BotonLeft4()
        {
            flagcarta = ArrayRanas[3];
            for (int i = 0; i < ArrayRanas.Length; i++)
            {
                if (ArrayRanas[i] == "vacio.png")
                {
                    z = i;
                    ArrayRanas[i] = flagcarta;
                }
            }
            ArrayRanas[3] = "vacio.png";
            AssignarLlocs();
        }
        public void BotonLeft5()
        {
            flagcarta = ArrayRanas[4];
            for (int i = 0; i < ArrayRanas.Length; i++)
            {
                if (ArrayRanas[i] == "vacio.png")
                {
                    z = i;
                    ArrayRanas[i] = flagcarta;
                }
            }
            ArrayRanas[4] = "vacio.png";
            AssignarLlocs();
        }
        public void BotonCentro()
        {
            flagcarta = ArrayRanas[5];
            for (int i = 0; i < ArrayRanas.Length; i++)
            {
                if (ArrayRanas[i] == "vacio.png")
                {
                    z = i;
                    ArrayRanas[i] = flagcarta;
                }
            }
            ArrayRanas[5] = "vacio.png";
            AssignarLlocs();
        }
        public void BotonRight1()
        {
            flagcarta = ArrayRanas[6];
            for (int i = 0; i < ArrayRanas.Length; i++)
            {
                if (ArrayRanas[i] == "vacio.png")
                {
                    z = i;
                    ArrayRanas[i] = flagcarta;
                }
            }
            ArrayRanas[6] = "vacio.png";
            AssignarLlocs();
        }
        public void BotonRight2()
        {
            flagcarta = ArrayRanas[7];
            for (int i = 0; i < ArrayRanas.Length; i++)
            {
                if (ArrayRanas[i] == "vacio.png")
                {
                    z = i;
                    ArrayRanas[i] = flagcarta;
                }
            }
            ArrayRanas[7] = "vacio.png";
            AssignarLlocs();
        }
        public void BotonRight3()
        {
            flagcarta = ArrayRanas[8];
            for (int i = 0; i < ArrayRanas.Length; i++)
            {
                if (ArrayRanas[i] == "vacio.png")
                {
                    z = i;
                    ArrayRanas[i] = flagcarta;
                }
            }
            ArrayRanas[8] = "vacio.png";
            AssignarLlocs();
        }
        public void BotonRight4()
        {
            flagcarta = ArrayRanas[9];
            for (int i = 0; i < ArrayRanas.Length; i++)
            {
                if (ArrayRanas[i] == "vacio.png")
                {
                    z = i;
                    ArrayRanas[i] = flagcarta;
                }
            }
            ArrayRanas[9] = "vacio.png";
            AssignarLlocs();
        }
        public void BotonRight5()
        {
            flagcarta = ArrayRanas[10];
            for (int i = 0; i < ArrayRanas.Length; i++)
            {
                if (ArrayRanas[i] == "vacio.png")
                {
                    z = i;
                    ArrayRanas[i] = flagcarta;
                }
            }
            ArrayRanas[10] = "vacio.png";
            AssignarLlocs();
        }
        public void EnableLLocs() 
        {
            //Declaro cada carta indicando quales se pueden activar a su lado
            for (i = 0; i < ArrayRanasBool.Length; i++)
            {
                if (ArrayRanas[0] == "vacio.png" )
                {
                    ArrayRanasBool[i] = false;
                    ArrayRanasBool[0] = false;
                    ArrayRanasBool[0 + 1] = true;
                    ArrayRanasBool[0 + 2] = true;
                }
                else if (ArrayRanas[1] == "vacio.png")
                {
                    ArrayRanasBool[i] = false;
                    ArrayRanasBool[1] = false;
                    ArrayRanasBool[1 - 1] = true;
                    ArrayRanasBool[1 + 1] = true;
                    ArrayRanasBool[1 + 2] = true;
                }
                else if (ArrayRanas[2] == "vacio.png")
                {
                    ArrayRanasBool[i] = false;
                    ArrayRanasBool[2 + 1] = true;
                    ArrayRanasBool[2 + 2] = true;
                    ArrayRanasBool[2 - 1] = true;
                    ArrayRanasBool[2 - 2] = true;
                }
                else if (ArrayRanas[3] == "vacio.png")
                {
                    ArrayRanasBool[i] = false;
                    ArrayRanasBool[3 + 1] = true;
                    ArrayRanasBool[3 + 2] = true;
                    ArrayRanasBool[3 - 1] = true;
                    ArrayRanasBool[3 - 2] = true;
                }
                else if (ArrayRanas[4] == "vacio.png")
                {
                    ArrayRanasBool[i] = false;
                    ArrayRanasBool[4 + 1] = true;
                    ArrayRanasBool[4 + 2] = true;
                    ArrayRanasBool[4 - 1] = true;
                    ArrayRanasBool[4 - 2] = true;
                }
                else if (ArrayRanas[5] == "vacio.png")
                {
                    ArrayRanasBool[i] = false;
                    ArrayRanasBool[5 + 1] = true;
                    ArrayRanasBool[5 + 2] = true;
                    ArrayRanasBool[5 - 1] = true;
                    ArrayRanasBool[5 - 2] = true;
                }
                else if (ArrayRanas[6] == "vacio.png")
                {
                    ArrayRanasBool[i] = false;
                    ArrayRanasBool[6 + 1] = true;
                    ArrayRanasBool[6 + 2] = true;
                    ArrayRanasBool[6 - 1] = true;
                    ArrayRanasBool[6 - 2] = true;
                }
                else if (ArrayRanas[7] == "vacio.png")
                {
                    ArrayRanasBool[i] = false;
                    ArrayRanasBool[7 + 1] = true;
                    ArrayRanasBool[7 + 2] = true;
                    ArrayRanasBool[7 - 1] = true;
                    ArrayRanasBool[7 - 2] = true;
                }
                else if (ArrayRanas[8] == "vacio.png")
                {
                    ArrayRanasBool[i] = false;
                    ArrayRanasBool[8 + 1] = true;
                    ArrayRanasBool[8 + 2] = true;
                    ArrayRanasBool[8 - 1] = true;
                    ArrayRanasBool[8 - 2] = true;
                }
                else if (ArrayRanas[9] == "vacio.png")
                {
                    ArrayRanasBool[i] = false;
                    ArrayRanasBool[9] = false;
                    ArrayRanasBool[9 + 1] = true;
                    ArrayRanasBool[9 - 1] = true;
                    ArrayRanasBool[9 - 2] = true;
                }
                else if(ArrayRanas[10] == "vacio.png")
                {
                    ArrayRanasBool[i] = false;
                    ArrayRanasBool[10] = false;
                    ArrayRanasBool[10 - 1] = true;
                    ArrayRanasBool[10 - 2] = true;
                }
                ArrayRanasBool[z] = false; //Rana actual desactivar
            }
            AssignarEnables(); //Llamo a la funcion que asigna cada array a la posicion
        }
        public void AssignarEnables()
        {
            //Asigno cada booleano a cada rana
            Left1Enable = ArrayRanasBool[0];
            Left2Enable = ArrayRanasBool[1];
            Left3Enable = ArrayRanasBool[2];
            Left4Enable = ArrayRanasBool[3];
            Left5Enable = ArrayRanasBool[4];
            CentroEnable = ArrayRanasBool[5];
            Right1Enable = ArrayRanasBool[6];
            Right2Enable = ArrayRanasBool[7];
            Right3Enable = ArrayRanasBool[8];
            Right4Enable = ArrayRanasBool[9];
            Right5Enable = ArrayRanasBool[10];
            ComprobarBe(); //Funcion que comprueba la posicion de las cartas
        }
        public void AssignarLlocs()
        {
            //Asignar posiciones
            Left1 = ArrayRanas[0];
            Left2 = ArrayRanas[1];
            Left3 = ArrayRanas[2];
            Left4 = ArrayRanas[3];
            Left5 = ArrayRanas[4];
            Centro = ArrayRanas[5];
            Right1 = ArrayRanas[6];
            Right2 = ArrayRanas[7];
            Right3 = ArrayRanas[8];
            Right4 = ArrayRanas[9];
            Right5 = ArrayRanas[10];
            flagcarta = "";
            NumClicks--; //Restar puntos en cada movimiento
            EnableLLocs();
        }
        public void ComprobarBe()
        {
            if (NumClicks == 0)
            {
                ImagenFondo = "gameover.png";
                Left1Enable = false;
                Left2Enable = false;
                Left3Enable = false;
                Left4Enable = false;
                Left5Enable = false;
                CentroEnable = false;
                Right1Enable = false;
                Right2Enable = false;
                Right3Enable = false;
                Right4Enable = false;
                Right5Enable = false;
                VisibleReset = true;
            }
            //Super if que comprueba todas las posiciones
            else if ((Left1 == "verd1.png" || Left1 == "verd2.png" || Left1 == "verd3.png" || Left1 == "verd4.png" || Left1 == "verd5.png") &&
                (Left2 == "verd1.png" || Left2 == "verd2.png" || Left2 == "verd3.png" || Left2 == "verd4.png" || Left2 == "verd5.png") &&
                (Left3 == "verd1.png" || Left3 == "verd2.png" || Left3 == "verd3.png" || Left3 == "verd4.png" || Left3 == "verd5.png") &&
                (Left4 == "verd1.png" || Left4 == "verd2.png" || Left4 == "verd3.png" || Left4 == "verd4.png" || Left4 == "verd5.png") &&
                (Left5 == "verd1.png" || Left5 == "verd2.png" || Left5 == "verd3.png" || Left5 == "verd4.png" || Left5 == "verd5.png") &&
                (Centro == "vacio.png") &&
                (Right1 == "blava1.png" || Right1 == "blava2.png" || Right1 == "blava3.png" || Right1 == "blava4.png" || Right1 == "blava5.png") &&
                (Right2 == "blava1.png" || Right2 == "blava2.png" || Right2 == "blava3.png" || Right2 == "blava4.png" || Right2 == "blava5.png") &&
                (Right3 == "blava1.png" || Right3 == "blava2.png" || Right3 == "blava3.png" || Right3 == "blava4.png" || Right3 == "blava5.png") &&
                (Right4 == "blava1.png" || Right4 == "blava2.png" || Right4 == "blava3.png" || Right4 == "blava4.png" || Right4 == "blava5.png") &&
                (Right5 == "blava1.png" || Right5 == "blava2.png" || Right5 == "blava3.png" || Right5 == "blava4.png" || Right5 == "blava5.png"))
            {
                //Si entra ganas el juego
                ImagenFondo = "win.png";
                Left1Enable = false;
                Left2Enable = false;
                Left3Enable = false;
                Left4Enable = false;
                Left5Enable = false;
                CentroEnable = false;
                Right1Enable = false;
                Right2Enable = false;
                Right3Enable = false;
                Right4Enable = false;
                Right5Enable = false;
                VisibleReset = true;
            }

        }

        public void ResetAll()
        {
            //Funcion que resetea todas las variables y arrays
            flagbooluna = true;
            String[] ArrayRanas = new String[11];
            Boolean[] ArrayRanasBool = new Boolean[11];
            FuncionUnaVez();
            BotonCentro();
            ImagenFondo = "fondo1.png";
            Left4Enable = true;
            Left5Enable = true;
            Right1Enable = true;
            Right2Enable = true;
            i = 0;
            z = 99;
            NumClicks = 50;
        }
    }
}
